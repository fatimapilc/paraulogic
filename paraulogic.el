;;; paraulogic --- Troba paraules del paraulogic

;;; Comentary: Necessita un bon diccionari

;;(setq dic '("orgull" "rotllo" "furor"))
(setq dic
      (with-current-buffer (find-file-noselect "~/catala.dic")
        (mapcan (lambda (x) (split-string x " " t))
                (split-string
                 (buffer-substring-no-properties (point-min) (point-max))
                 "\n"))))

;; Les lletres del paraulogic
(setq good '("r" "o" "m" "l" "s" "u" "g"))

;; La rutina treu el resultat pel buffer *Messages*
(dolist (p dic)
  (setq p-yes t)
  (dolist (c (append p nil))
    (setq c-yes nil)
    (dolist (g good)
      (when (string= (string c) g)
    	(setq c-yes t)))
    (when (not c-yes)
      (progn (setq p-yes nil)
             (cl-return))))
  (if p-yes
      (message "PARAULA YES: %s" p)))
